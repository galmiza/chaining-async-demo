# Welcome

This snippet demonstrates how to implement a simple and efficient sequential execution chain of asynchronous functions.  
As in a chain, the response of a function is sent as input to the next function.


# Why using a chain?

Let's consider the three following async functions that perform respectively an async addition, an async substraction and an async multiplication of an 'input' parameter with a 'value' parameter.

```javascript
var addAsync = function(input,value,callback) {
  callback(null,input+value);
}
var subAsync = function(input,value,callback) {
  callback(null,input-value);
}
var mulAsync = function(input,value,callback) {
  callback(null,input*value);
}
```

To call sequentially these functions and pass the result of one to the next, the code would look like:

```javascript
addAsync(2,10,function(err,res) {         // start with value 2, then add 10
  subAsync(res,5,function(err,res) {      // then substract 5
    mulAsync(res,3,function(err,res) {    // then multiply by 3
      addAsync(res,6,function(err,res) {  // then substract 3
        mulAsync(res,2,console.log);      // then multiply by 2
      });
    });
  });
});
```

Using a chain, the code gets much simpler:

```javascript
new MySimpleChain().add(10).sub(5).mul(3).add(6).mul(2).done(2,console.log);
```

In this example, the functions addAsync, subAsync and mulAsync have be "linked" respectively to the methods add, sub and mul of the MySimpleChain class.


# Getting started

* Create a class that inherits the "Chain" class (as MySimpleChain in the snippet)
* Add methods to that class that return a new instance of the class where an execution request is added (see example)

An execution request is defined by 3 attributes :

* f, reference to an async function taking 3 parameters
    * input, result of last execution
    * params, see just below
    * test, see just below 
  
* params, the parameter for the execution
* test, an optional function to be evaluate to break the chain execution  

_Example_

```javascript

// Definition of our new class
var MySimpleChain = function(stack,init) { 
  s = stack || [];
  Chain.call(this,stack,init); // inherit class "Chain"
  
  // Add the 3 methods add,sub,mul
  // (they can be seen as wrappers for the real async functions)
  // (note that method add queues an additional parameter to define a test function able to break the execution)
  this.add = function(value,f) {
    return new MySimpleChain(s).queue({f:addAsync,params:value,test:f});
  }
  this.sub = function(value) {
    return new MySimpleChain(s).queue({f:subAsync,params:value});
  }
  this.mul = function(value) {
    return new MySimpleChain(s).queue({f:mulAsync,params:value});
  }
  return this;
}

// Create an execute the chain
new MySimpleChain()
 .add(10, function(i){return i==0}) // break the chain execution if the result at this stage is 0
 .sub(5)
 .mul(3)
 .done(2,console.log); // set the initial input as 2 and output execution result to the console
```

